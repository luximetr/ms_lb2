//
//  ViewController.swift
//  lb2
//
//  Created by Александр on 06.03.17.
//  Copyright © 2017 Alexandr Orlov. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    //MARK: IBOutlets
    @IBOutlet weak var valueLabel: UILabel!
    @IBOutlet weak var slider: UISlider!
    
    //MARK: Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        valueLabel.text = String(slider.value)
    }

    //MARK: IBActions
    @IBAction func sliderDidChange(_ sender: Any) {
        valueLabel.text = String(slider.value)
    }

}

