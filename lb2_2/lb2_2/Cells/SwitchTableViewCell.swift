//
//  SwitchTableViewCell.swift
//  lb2_2
//
//  Created by Александр on 06.03.17.
//  Copyright © 2017 Alexandr Orlov. All rights reserved.
//

import UIKit

class SwitchTableViewCell: UITableViewCell, TableViewCellProtocol {

    @IBOutlet weak var cellNumberLabel: UILabel!
    @IBOutlet weak var userTextLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configure(with text: String, at indexPath: IndexPath) {
        userTextLabel.text = text
        cellNumberLabel.text = "Секция: \(indexPath.section), ряд \(indexPath.row)"
    }

}
