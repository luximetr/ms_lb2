//
//  TableViewCellProtocol.swift
//  lb2_2
//
//  Created by Александр on 06.03.17.
//  Copyright © 2017 Alexandr Orlov. All rights reserved.
//

import UIKit

protocol TableViewCellProtocol {
    func configure(with text: String, at indexPath: IndexPath)
}
