//
//  ViewController.swift
//  lb2_2
//
//  Created by Александр on 06.03.17.
//  Copyright © 2017 Alexandr Orlov. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    //MARK: IBOutlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var addCellButton: UIButton!
    @IBOutlet weak var textField: UITextField!
    
    //MARK: Private  Properties
    var numberOfCells: Int = 10
    
    //MARK: Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(UINib.init(nibName: "SwitchTableViewCell", bundle: nil), forCellReuseIdentifier: "SwitchTableViewCellIdentifier")
        tableView.register(UINib.init(nibName: "SliderTableViewCell", bundle: nil), forCellReuseIdentifier: "SliderTableViewCellIdentifier")
    }
    
    //MARK: IBActions
    
    @IBAction func tapOnAddCellButton(_ sender: Any) {
        numberOfCells += 1
        tableView.insertRows(at: [IndexPath.init(row: 0, section: 0)], with: UITableViewRowAnimation.middle)
    }
    
    //MARK: UITableViewDataSource
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return numberOfCells
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let identifier = (indexPath.row % 2 == 0) ? "SliderTableViewCellIdentifier" : "SwitchTableViewCellIdentifier"
        
        let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as!TableViewCellProtocol
        
        cell.configure(with: textField.text!, at: indexPath)
        return cell as! UITableViewCell
    }
    
    //MARK: UITableViewDelegate
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        numberOfCells -= 1
        tableView.deleteRows(at: [indexPath], with: UITableViewRowAnimation.middle)
    }
}

